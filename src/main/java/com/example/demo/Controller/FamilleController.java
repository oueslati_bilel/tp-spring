package com.example.demo.Controller;

import com.example.demo.Entity.F_ARTICLE;
import com.example.demo.Entity.F_FAMILLE;
import com.example.demo.Repository.FFamillleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class FamilleController {

    @Autowired
    FFamillleRepository fFamillleRepository;

    @GetMapping("/famille")
    public List<F_FAMILLE> getAllFamily() {
        return fFamillleRepository.findAll();
    }
}
