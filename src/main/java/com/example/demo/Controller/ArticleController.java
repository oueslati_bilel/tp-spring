package com.example.demo.Controller;


import java.util.ArrayList;
import java.util.List;

import com.example.demo.Entity.F_ARTSTOCK;
import com.example.demo.Entity.F_DEPOT;
import com.example.demo.Repository.ArtStockRepository;
import com.example.demo.Repository.FDepotRepository;
import com.example.demo.payload.ArticleResponse;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import com.example.demo.Entity.F_ARTICLE;
import com.example.demo.Repository.ArticleRepository;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")

public class ArticleController {
	@Autowired
	ArticleRepository articleRepository;
	@Autowired
	ArtStockRepository artStockRepository;
	@Autowired
	FDepotRepository fDepotRepository;

	@GetMapping("/article")
	public List<ArticleResponse> getAllArticle() {
		List<F_ARTICLE> articles = articleRepository.findAllByFamille();
		List<ArticleResponse> responses = new ArrayList<>();
		for(int i =0 ; i<articles.size(); i++){
			ArticleResponse response = new ArticleResponse();
			response.setAr_Ref(articles.get(i).getAR_Ref());
			response.setAr_Design(articles.get(i).getAR_Design());
			response.setAr_PrixAch(articles.get(i).getAR_PrixAch());
			response.setAr_PrixVen(articles.get(i).getAR_PrixVen());
			response.setFa_Intitule(articles.get(i).getFA_CodeFamille());

			responses.add(response);
		}
		/*articles.forEach(f_article -> {
			ArticleResponse response = new ArticleResponse();
			response.setAr_Ref(f_article.getAR_Ref());
			response.setAr_Design(f_article.getAR_Design());
			response.setAr_PrixAch(f_article.getAR_PrixAch());
			response.setAr_PrixVen(f_article.getAR_PrixVen());
			response.setFa_Intitule(f_article.getFA_CodeFamille());
			*//*response.setDe_Intitule(f_article.getStocks().get(f_article.getCBMarq()).getF_depot().getDE_Intitule());
			response.setAs_qteSto(f_article.getStocks().get(f_article.getCBMarq()).getAS_QteSto());
			response.setAs_MontSto(f_article.getStocks().get(f_article.getCBMarq()).getAS_MontSto());*//*
			responses.add(response);
		});*/
			return responses;
	}

	@GetMapping("/stock")
	public List<ArticleResponse> getAllStock() {
		List<F_ARTSTOCK> artstocks = artStockRepository.findAll();
		List<ArticleResponse> responses = new ArrayList<>();
		for(int i =0 ; i<artstocks.size(); i++){
			ArticleResponse response = new ArticleResponse();
			response.setAr_Ref(artstocks.get(i).getArticle().getAR_Ref());
			response.setAr_Design(artstocks.get(i).getArticle().getAR_Design());
			response.setAr_PrixAch(artstocks.get(i).getArticle().getAR_PrixAch());
			response.setAr_PrixVen(artstocks.get(i).getArticle().getAR_PrixVen());
			response.setFa_Intitule(artstocks.get(i).getArticle().getFA_CodeFamille());
			response.setAs_qteSto(artstocks.get(i).getAS_QteSto());
			response.setAs_MontSto(artstocks.get(i).getAS_MontSto());
			response.setDe_Intitule(artstocks.get(i).getF_depot().getDE_Intitule());
			responses.add(response);

		}
		return responses;
	}


	@GetMapping("/depot")
	public List<F_DEPOT> getAllDepot() {
		return fDepotRepository.findAll();
	}

	@GetMapping("/article/{id}")
	public F_ARTICLE getArticle(@PathVariable int id) {
		return articleRepository.findById(id).get();
	}

/*	@GetMapping("/art")
	public List<F_ARTICLE>getArticle(){
		return articleRepository.findByAR_RefAndAR_DesignOrderByFA_CodeFamille();
	}*/
}
