package com.example.demo.Controller;


import com.example.demo.Entity.F_COLLABORATEUR;
import com.example.demo.Repository.CollaborateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/collaborateur")
public class CollaborateurController {
    @Autowired
    CollaborateurRepository collaborateurRepository;

    @GetMapping("/get")
    public List<F_COLLABORATEUR> getAllCollab() {
        return collaborateurRepository.findAll();
    }
}
