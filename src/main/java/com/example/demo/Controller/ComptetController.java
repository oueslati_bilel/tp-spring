package com.example.demo.Controller;


import com.example.demo.Entity.F_ARTICLE;
import com.example.demo.Entity.F_COMPTET;
import com.example.demo.Repository.ComptetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/comptet")
public class ComptetController {
    @Autowired
    ComptetRepository comptetRepository;

    @GetMapping("/list")
    public List<F_COMPTET> getAllComptet() {
        return comptetRepository.findComptetByClient();
    }
}
