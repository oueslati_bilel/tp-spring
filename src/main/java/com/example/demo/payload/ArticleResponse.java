package com.example.demo.payload;

import lombok.Data;

@Data
public class ArticleResponse {
    private String ar_Ref;
    private String ar_Design;
    private double ar_PrixAch;
    private double ar_PrixVen;
    private String fa_Intitule;
    private String de_Intitule;
    private double as_qteSto;
    private double as_MontSto;




}
