package com.example.demo.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "F_DEPOT")
@Data
@AllArgsConstructor @NoArgsConstructor

public class F_DEPOT  implements Serializable {

    @Id
    @Column(name="cbmarq")
    int CBMarq;


    @Column(name="DE_No")
    int DE_No;

    @Column(name="DE_Intitule")
    String DE_Intitule;

    @JsonIgnore
    @OneToMany
    private List<F_ARTSTOCK> stocks;

/*
    @Column(name="DE_No")
    int DE_No;

    @Column(name="DE_Intitule")
    String DE_Intitule;

    @OneToMany(mappedBy="f_depot", fetch=FetchType.LAZY)
    private List<F_ARTSTOCK> artstocks;

    public int getCBMarq() {
        return CBMarq;
    }

    public void setCBMarq(int CBMarq) {
        this.CBMarq = CBMarq;
    }

    public int getDE_No() {
        return DE_No;
    }

    public void setDE_No(int DE_No) {
        this.DE_No = DE_No;
    }

    public String getDE_Intitule() {
        return DE_Intitule;
    }

    public void setDE_Intitule(String DE_Intitule) {
        this.DE_Intitule = DE_Intitule;
    }

    public List<F_ARTSTOCK> getArtstocks() {
        return artstocks;
    }

    public void setArtstocks(List<F_ARTSTOCK> artstocks) {
        this.artstocks = artstocks;
    }

    public F_DEPOT() {
    }

    public F_DEPOT(int CBMarq, int DE_No, String DE_Intitule, List<F_ARTSTOCK> artstocks) {
        this.CBMarq = CBMarq;
        this.DE_No = DE_No;
        this.DE_Intitule = DE_Intitule;
        this.artstocks = artstocks;
    }

    @Override
    public String toString() {
        return "F_DEPOT{" +
                "CBMarq=" + CBMarq +
                ", DE_No=" + DE_No +
                ", DE_Intitule='" + DE_Intitule + '\'' +
                ", artstocks=" + artstocks +
                '}';
    }*/
}
