package com.example.demo.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "F_FAMILLE")
@Data
@AllArgsConstructor @NoArgsConstructor
public class F_FAMILLE implements Serializable {

    @Id
    @Column(name="cbmarq")
    int CBMarq;

    @Column(name="FA_CodeFamille")
    String FA_CodeFamille;

    @Column(name="FA_Intitule")
    String FA_Intitule;

    @JsonIgnore
    @OneToMany(mappedBy="f_famille", fetch=FetchType.LAZY)
    private List<F_ARTICLE> articles;



   /* @Column(name="FA_CodeFamille")
    String FA_CodeFamille;

    @Column(name="FA_Intitule")
    String FA_Intitule;

    @OneToMany(mappedBy="f_famille", fetch=FetchType.LAZY)
    private List<F_ARTICLE> articles;

    public int getCBMarq() {
        return CBMarq;
    }

    public void setCBMarq(int CBMarq) {
        this.CBMarq = CBMarq;
    }

    public String getFA_CodeFamille() {
        return FA_CodeFamille;
    }

    public void setFA_CodeFamille(String FA_CodeFamille) {
        this.FA_CodeFamille = FA_CodeFamille;
    }

    public String getFA_Intitule() {
        return FA_Intitule;
    }

    public void setFA_Intitule(String FA_Intitule) {
        this.FA_Intitule = FA_Intitule;
    }

    public List<F_ARTICLE> getArticles() {
        return articles;
    }

    public void setArticles(List<F_ARTICLE> articles) {
        this.articles = articles;
    }

    public F_FAMILLE() {
    }

    public F_FAMILLE(int CBMarq, String FA_CodeFamille, String FA_Intitule, List<F_ARTICLE> articles) {
        this.CBMarq = CBMarq;
        this.FA_CodeFamille = FA_CodeFamille;
        this.FA_Intitule = FA_Intitule;
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "F_FAMILLE{" +
                "CBMarq=" + CBMarq +
                ", FA_CodeFamille='" + FA_CodeFamille + '\'' +
                ", FA_Intitule='" + FA_Intitule + '\'' +
                ", articles=" + articles +
                '}';
    }*/
}
