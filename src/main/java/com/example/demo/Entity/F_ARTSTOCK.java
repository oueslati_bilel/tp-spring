package com.example.demo.Entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "F_ARTSTOCK")
@Data
public class F_ARTSTOCK implements Serializable {
    @Id
    @Column(name="cbmarq")
    int CBMarq;

    @Column(name="AS_QteSto")
    double AS_QteSto;

    @Column(name="AS_MontSto")
    double AS_MontSto;




    @ManyToOne
    @JoinColumn(name="AR_Ref",insertable = false, updatable = false, referencedColumnName = "AR_Ref")
    private  F_ARTICLE article;


    @ManyToOne
    @JoinColumn(name="DE_No", referencedColumnName = "DE_No")
    private F_DEPOT f_depot;

/*
    @Column(name="AS_QteSto")
    double AS_QteSto;

    @Column(name="AS_MontSto")
    double AS_MontSto;


  *//*  @OneToMany(mappedBy="f_artstock", fetch=FetchType.LAZY)
    private List<F_ARTICLE> articles;*//*

    @ManyToOne
    @JoinColumn(name="DE_No")
    private F_DEPOT f_depot;

    public int getCBMarq() {
        return CBMarq;
    }

    public void setCBMarq(int cbMarq) {
        this.CBMarq = cbMarq;
    }

    public double getAS_QteSto() {
        return AS_QteSto;
    }

    public void setAS_QteSto(double AS_QteSto) {
        this.AS_QteSto = AS_QteSto;
    }

    public double getAS_MontSto() {
        return AS_MontSto;
    }

    public void setAS_MontSto(double AS_MontSto) {
        this.AS_MontSto = AS_MontSto;
    }

    public List<F_ARTICLE> getArticles() {
        return articles;
    }

    public void setArticles(List<F_ARTICLE> articles) {
        this.articles = articles;
    }

    public F_ARTSTOCK() {
    }

    public F_ARTSTOCK(int cbMarq, double AS_QteSto, double AS_MontSto, List<F_ARTICLE> articles) {
        this.CBMarq = cbMarq;
        this.AS_QteSto = AS_QteSto;
        this.AS_MontSto = AS_MontSto;
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "F_ARTSTOCK{" +
                "cbMarq=" + CBMarq +
                ", AS_QteSto=" + AS_QteSto +
                ", AS_MontSto=" + AS_MontSto +
                ", articles=" + articles +
                '}';
    }*/
}
