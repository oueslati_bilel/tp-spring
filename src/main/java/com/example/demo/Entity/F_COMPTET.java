package com.example.demo.Entity;


import javax.persistence.*;

@Entity
@Table(name = "F_COMPTET")
public class F_COMPTET {
    @Id
    @Column(name="cbmarq")
    int CBMarq;

    @Column(name="CT_Num")
    String CT_Num;

    @Column(name="CT_Intitule")
    String CT_Intitule;

    @Column(name="CT_Adresse")
    String CT_Adresse;

    @Column(name="CT_CodeRegion")
    String CT_CodeRegion;


    @Column(name="CO_NO")
    String CO_NO;
    @Column(name="CO_Nom")
    String CO_Nom;

    @Column(name="CO_Prenom")
    String CO_Prenom;

    @ManyToOne
    @JoinColumn(name="CO_No")
    private F_COLLABORATEUR f_collaborateur;

    public int getCBMarq() {
        return CBMarq;
    }

    public void setCBMarq(int CBMarq) {
        this.CBMarq = CBMarq;
    }

    public String getCT_Num() {
        return CT_Num;
    }

    public void setCT_Num(String CT_Num) {
        this.CT_Num = CT_Num;
    }

    public String getCT_Intitule() {
        return CT_Intitule;
    }

    public void setCT_Intitule(String CT_Intitule) {
        this.CT_Intitule = CT_Intitule;
    }

    public String getCT_Adresse() {
        return CT_Adresse;
    }

    public void setCT_Adresse(String CT_Adresse) {
        this.CT_Adresse = CT_Adresse;
    }

    public String getCT_CodeRegion() {
        return CT_CodeRegion;
    }

    public void setCT_CodeRegion(String CT_CodeRegion) {
        this.CT_CodeRegion = CT_CodeRegion;
    }

    public String getCO_NO() {
        return CO_NO;
    }

    public void setCO_NO(String CO_NO) {
        this.CO_NO = CO_NO;
    }

    public String getCO_Nom() {
        return CO_Nom;
    }

    public void setCO_Nom(String CO_Nom) {
        this.CO_Nom = CO_Nom;
    }

    public String getCO_Prenom() {
        return CO_Prenom;
    }

    public void setCO_Prenom(String CO_Prenom) {
        this.CO_Prenom = CO_Prenom;
    }

    public F_COLLABORATEUR getF_collaborateur() {
        return f_collaborateur;
    }

    public void setF_collaborateur(F_COLLABORATEUR f_collaborateur) {
        this.f_collaborateur = f_collaborateur;
    }

    public F_COMPTET() {
    }

    public F_COMPTET(int CBMarq, String CT_Num, String CT_Intitule, String CT_Adresse, String CT_CodeRegion, String CO_NO, String CO_Nom, String CO_Prenom, F_COLLABORATEUR f_collaborateur) {
        this.CBMarq = CBMarq;
        this.CT_Num = CT_Num;
        this.CT_Intitule = CT_Intitule;
        this.CT_Adresse = CT_Adresse;
        this.CT_CodeRegion = CT_CodeRegion;
        this.CO_NO = CO_NO;
        this.CO_Nom = CO_Nom;
        this.CO_Prenom = CO_Prenom;
        this.f_collaborateur = f_collaborateur;
    }

    @Override
    public String toString() {
        return "F_COMPTET{" +
                "CBMarq=" + CBMarq +
                ", CT_Num='" + CT_Num + '\'' +
                ", CT_Intitule='" + CT_Intitule + '\'' +
                ", CT_Adresse='" + CT_Adresse + '\'' +
                ", CT_CodeRegion='" + CT_CodeRegion + '\'' +
                ", CO_NO='" + CO_NO + '\'' +
                ", CO_Nom='" + CO_Nom + '\'' +
                ", CO_Prenom='" + CO_Prenom + '\'' +
                ", f_collaborateur=" + f_collaborateur +
                '}';
    }
}
