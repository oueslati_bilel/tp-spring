package com.example.demo.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "F_COLLABORATEUR")
public class F_COLLABORATEUR {

    @Id
    @Column(name="cbmarq")
    int CBMarq;

    @Column(name="CO_Nom")
    String CO_Nom;

    @Column(name="CO_Prenom")
    String CO_Prenom;

    @Column(name="CO_NO")
    int CO_NO;

    @OneToMany(mappedBy="f_collaborateur", fetch=FetchType.LAZY)
    private List<F_COMPTET> f_comptets;

    public int getCO_NO() {
        return CO_NO;
    }

    public void setCO_NO(int CO_NO) {
        this.CO_NO = CO_NO;
    }

    public int getCbMarq() {
        return CBMarq;
    }

    public void setCbMarq(int cbMarq) {
        this.CBMarq = cbMarq;
    }

    public String getCO_Nom() {
        return CO_Nom;
    }

    public void setCO_Nom(String CO_Nom) {
        this.CO_Nom = CO_Nom;
    }

    public String getCO_Prenom() {
        return CO_Prenom;
    }

    public void setCO_Prenom(String CO_Prenom) {
        this.CO_Prenom = CO_Prenom;
    }

    public F_COLLABORATEUR(int cbMarq, String CO_Nom, String CO_Prenom,int CO_NO) {
        this.CBMarq = CBMarq;
        this.CO_Nom = CO_Nom;
        this.CO_Prenom = CO_Prenom;
        this.CO_NO = CO_NO;
    }

    public F_COLLABORATEUR() {

    }

    @Override
    public String toString() {
        return "F_COLLABORATEUR{" +
                "cbMarq=" + CBMarq +
                ", CO_Nom='" + CO_Nom + '\'' +
                ", CO_Prenom='" + CO_Prenom + '\'' +
                ", CO_NO=" + CO_NO +
                '}';
    }
}
