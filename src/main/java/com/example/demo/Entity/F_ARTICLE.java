package com.example.demo.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.source.doctree.SerialDataTree;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "F_ARTICLE")
@Data
@AllArgsConstructor @NoArgsConstructor
public class F_ARTICLE implements Serializable {
	@Id
	@Column(name="cbmarq")
	int CBMarq;

	@Column(name ="AR_Ref")
	String AR_Ref;

	@Column(name ="AR_Design")
	String AR_Design;

	@Column(name ="FA_CodeFamille")
	String FA_CodeFamille;

	@Column(name ="AR_PrixAch")
	float AR_PrixAch;

	@Column(name ="AR_PrixVen")
	double AR_PrixVen;

	@ManyToOne
	@JoinColumn(name="FA_CodeFamille",insertable = false, updatable = false, referencedColumnName = "FA_CodeFamille")
	private F_FAMILLE f_famille;

	@JsonIgnore
	@OneToMany(mappedBy="article", fetch=FetchType.LAZY)
	private List<F_ARTSTOCK> stocks;




	/*@Column(name ="AR_Ref")
	String AR_Ref;
	
	@Column(name ="AR_Design")
	String AR_Design;
	
	@Column(name ="FA_CodeFamille")
	String FA_CodeFamille;
	
	@Column(name ="AR_PrixAch")
	float AR_PrixAch;
	
	@Column(name ="AR_PrixVen")
	double AR_PrixVen;

	@ManyToOne
	@JoinColumn(name="AR_Ref" ,insertable = false, updatable = false)
	private F_ARTSTOCK f_artstock;

	@ManyToOne
	@JoinColumn(name="FA_CodeFamille",insertable = false, updatable = false)
	private F_FAMILLE f_famille;


	public int getCBMarq() {
		return CBMarq;
	}

	public void setCBMarq(int cbMarq) {
		this.CBMarq = cbMarq;
	}

	public String getAR_Ref() {
		return AR_Ref;
	}

	public void setAR_Ref(String AR_Ref) {
		this.AR_Ref = AR_Ref;
	}

	public String getAR_Design() {
		return AR_Design;
	}

	public void setAR_Design(String AR_Design) {
		this.AR_Design = AR_Design;
	}

	public String getFA_CodeFamille() {
		return FA_CodeFamille;
	}

	public void setFA_CodeFamille(String FA_CodeFamille) {
		this.FA_CodeFamille = FA_CodeFamille;
	}

	public float getAR_PrixAch() {
		return AR_PrixAch;
	}

	public void setAR_PrixAch(float AR_PrixAch) {
		this.AR_PrixAch = AR_PrixAch;
	}

	public double getAR_PrixVen() {
		return AR_PrixVen;
	}

	public void setAR_PrixVen(double AR_PrixVen) {
		this.AR_PrixVen = AR_PrixVen;
	}

	public F_ARTSTOCK getF_artstock() {
		return f_artstock;
	}

	public void setF_artstock(F_ARTSTOCK f_artstock) {
		this.f_artstock = f_artstock;
	}

	public F_ARTICLE(int CBMarq, String AR_Ref, String AR_Design, String FA_CodeFamille, float AR_PrixAch, double AR_PrixVen, F_ARTSTOCK f_artstock) {
		this.CBMarq = CBMarq;
		this.AR_Ref = AR_Ref;
		this.AR_Design = AR_Design;
		this.FA_CodeFamille = FA_CodeFamille;
		this.AR_PrixAch = AR_PrixAch;
		this.AR_PrixVen = AR_PrixVen;
		this.f_artstock = f_artstock;
	}

	public F_ARTICLE() {
	}

	@Override
	public String toString() {
		return "F_ARTICLE{" +
				"cbMarq=" + CBMarq +
				", AR_Ref='" + AR_Ref + '\'' +
				", AR_Design='" + AR_Design + '\'' +
				", FA_CodeFamille='" + FA_CodeFamille + '\'' +
				", AR_PrixAch=" + AR_PrixAch +
				", AR_PrixVen=" + AR_PrixVen +
				", f_artstock=" + f_artstock +
				'}';
	}*/
}
