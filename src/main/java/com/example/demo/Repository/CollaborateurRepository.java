package com.example.demo.Repository;

import com.example.demo.Entity.F_COLLABORATEUR;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CollaborateurRepository extends JpaRepository<F_COLLABORATEUR,Long> {
}
