package com.example.demo.Repository;


import com.example.demo.Entity.F_ARTICLE;
import com.example.demo.Entity.F_ARTSTOCK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ArtStockRepository extends JpaRepository<F_ARTSTOCK, Integer> {



}
