package com.example.demo.Repository;


import com.example.demo.Entity.F_ARTICLE;
import com.example.demo.Entity.F_DEPOT;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FDepotRepository extends JpaRepository<F_DEPOT, Integer>{

}
