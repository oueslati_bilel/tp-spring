package com.example.demo.Repository;

import com.example.demo.Entity.F_ARTICLE;
import com.example.demo.Entity.F_FAMILLE;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FFamillleRepository extends JpaRepository<F_FAMILLE, Integer> {
}
