package com.example.demo.Repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.Entity.F_ARTICLE;

import java.util.List;

@Repository
public interface ArticleRepository extends JpaRepository<F_ARTICLE, Integer>{
    @Query(value="select A.CBMarq,A.AR_Ref ,A.AR_Design,A.AR_PrixAch,A.AR_PrixVen,F.FA_Intitule,D.DE_Intitule,S.AS_QteSto,S.AS_MontSto,A.FA_CodeFamille\n" +
            "from F_ARTICLE A , F_FAMILLE F,F_DEPOT D,F_ARTSTOCK S " +
            "where A.FA_CodeFamille=F.FA_CodeFamille  and D.DE_No=S.DE_No  and S.AR_Ref=A.AR_Ref and D.DE_No = 1\n" +
            "group by F.FA_Intitule,A.CBMarq,A.AR_Ref,A.AR_Design,A.AR_PrixAch,A.AR_PrixVen,D.DE_Intitule,S.AS_QteSto,S.AS_MontSto,A.FA_CodeFamille",nativeQuery = true)
    List<F_ARTICLE> findAllByFamille();

}
