package com.example.demo.Repository;

import com.example.demo.Entity.F_COMPTET;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ComptetRepository extends JpaRepository<F_COMPTET,Long> {

    @Query(value = "SELECT C.CBMarq,C.CT_Num,C.CT_Intitule,C.CT_Adresse,C.CO_No,C.CT_CodeRegion,CL.CO_Nom,CL.CO_Prenom\n" +
            "FROM F_COMPTET C,F_COLLABORATEUR CL \n" +
            "where (C.CO_No=CL.CO_No) and(C.CT_Type=0)",nativeQuery = true)
    List<F_COMPTET>findComptetByClient();
}
